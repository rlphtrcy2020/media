<?php
/**
 * @file
 * Setting form file for the module.
 */

/**
 * Implements hook_settigs_form().
 */
function vimeovideogallery_settings_form() {
  $form = array();
  $form['vimeovideogallery'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vimeo Video Gallery settings'),
    '#collapsible' => FALSE,
  );

  $form['vimeovideogallery']['vimeovideogallery_from'] = array(
    '#type' => 'select',
    '#title' => t('Display Vimeo Video From'),
    '#options' => array(
      'user' => t('User'),
      'album' => t('Album'),
      'group' => t('Group'),
      'channel' => t('Channel'),
    ),
    '#required' => TRUE,
    '#attributes' => array('class' => array('vimeo-video-gallery-from')),
    '#default_value' => variable_get('vimeovideogallery_from', NULL),
    '#description' => t('Select option from where you want to display video (e.g. from Vimeo user or album or group or channel).'),
  );
  $form['vimeovideogallery']['vimeovideogallery_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#size' => 40,
    '#default_value' => variable_get('vimeovideogallery_key', NULL),
    '#required' => TRUE,
    '#attributes' => array('class' => array('vimeo-video-gallery-key')),
    '#description' => t('The Vimeo user or album or group or channel name you want to get the videos.'),
  );
  return system_settings_form($form);
}

/**
 * Implements hook_settings_form_validate().
 */
function vimeovideogallery_settings_form_validate($form, $form_state) {
  if ($form_state['values']['vimeovideogallery_from'] == 'channel') {
        if (is_nan($form_state['values']['vimeovideogallery_key'])) {
            form_set_error('config_vimeovideogallery_key', t('You must enter a valid channel number.'));
        }
    }
    elseif (is_numeric($form_state['values']['vimeovideogallery_key'])) {
        form_set_error('config_vimeovideogallery_key', t('You must enter a valid name.'));
    }
}
