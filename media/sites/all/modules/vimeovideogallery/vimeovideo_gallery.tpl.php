<?php
/**
 * @file
 * Theme file to display youtubechannel.
 */
if ($vimeo_content) :
?>
    <div id="wrapper">
        <div id="embed">
          <iframe id="vimeo-frame" frameborder="0" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" title="" src=""></iframe>
        </div>
        <div class="clear"></div>
        <div id="thumbs" class="vimeo-video-list">
            <ul>
                <?php foreach ($vimeo_content as $key => $value) : ?>
                    <li>
                        <a href="<?php echo $value['video_url'] ?>" id="<?php echo $key; ?>" class="<?php echo $key; ?>">
                            <?php echo $value['video_thumb']; ?>
                            <p><?php echo $value['video_title']; ?></p>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
            <div class="clear"></div>
        </div>
        <div id="buttons">
            <a href="#" id="prev">&#x276e;</a>
            <a href="#" id="next">&#10095;</a>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
<?php
else :
?>
  <h3><?php print t('Could not fetch videos from Vimeo.'); ?></h3>
<?php
endif;
?>
