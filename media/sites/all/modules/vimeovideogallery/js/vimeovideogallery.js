/**
 * @file
 * Code for vimeo video slider.
 */

(function ($) {
  'use strict';
  function vimeo_setvideo(vimeo_id) {
    jQuery('#vimeo-frame').attr('src', 'https://player.vimeo.com/video/' + vimeo_id);
  }

  Drupal.behaviors.vimeovideogallery = {
    attach: function (context, settings) {
      if (jQuery('#thumbs a:first').length) {
        vimeo_setvideo(jQuery('#thumbs a:first').attr('id'));
        jQuery('#thumbs a').click(function (e) {
          vimeo_setvideo(jQuery(this).attr('id'));
          return false;
        });
      }
      var run = setInterval(rotate, 5000);
      var item_width = jQuery('#thumbs li').outerWidth();
      var item_count = jQuery ('#thumbs li').length;
      if (item_count * (item_width + 10) > jQuery('#thumbs ul').outerWidth()){
        var left_value = item_width * (-1);
      }
      else{
        var left_value = 0;
      }
      var vimeo_block_width = jQuery('.block-vimeovideogallery').width();
      if (vimeo_block_width >= 690) {
        jQuery('.block-vimeovideogallery #wrapper').css('width', '75%');
      }
      jQuery('#thumbs li:first').before(jQuery('#thumbs li:last'));
      jQuery('#thumbs ul').css('left', left_value);
      jQuery('#prev').click(function () {
        var left_indent = parseInt(jQuery('#thumbs ul').css('left')) + item_width;
        jQuery('#thumbs ul').animate({left: left_indent}, 200, function () {
          jQuery('#thumbs li:first').before(jQuery('#thumbs li:last'));
          jQuery('#thumbs ul').css('left', left_value);
        });
        return false;
      });
      jQuery('#next').click(function () {
        var left_indent = parseInt(jQuery('#thumbs ul').css('left')) - item_width;
        jQuery('#thumbs ul').animate({left: left_indent}, 200, function () {
          jQuery('#thumbs li:last').after(jQuery('#thumbs li:first'));
          jQuery('#thumbs ul').css('left', left_value);
        });
        return false;
      });
      jQuery('#thumbs').hover(
        function () {
          clearInterval(run);
        },
        function () {
          run = setInterval(rotate, 5000);
        }
      );
    }
  };
}(jQuery));


/**
 * A simple function to click next link.
 */
function rotate() {
  'use strict';
  jQuery('#next').click();
}
