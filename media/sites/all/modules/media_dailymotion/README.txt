= Media: Dailymotion =

Creates a Dailymotion PHP Stream Wrapper and implements Media Internet
Sources API to let insert dailymotion videos. The output is handled by
the integration with file_entity module.

By default module saves file with hash of video in filename. But you
can set variable media_dailymotion__title_to_filename to TRUE.
And module will save file with title of video in filename.
But also module will make one additional request to service on saving asset.
