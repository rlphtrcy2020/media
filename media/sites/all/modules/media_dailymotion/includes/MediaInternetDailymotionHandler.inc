<?php
/**
 * @file
 *   media_internet handler for dailymotion.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetDailymotionHandler extends MediaInternetBaseHandler {
  public function parse($embedCode) {
    $patterns = array(
      '@dailymotion\.com/swf/([^"\&]+)@i',
      '@dailymotion\.com/video/([^/_]+)_@i',
      '@dailymotion\.com/video/([^/_]+)@i',
      '@dai\.ly/([^"\&\? ]+)@i',
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      if (isset($matches[1])) {
        return file_stream_wrapper_uri_normalize('dailymotion://video_id/' . $matches[1]);
      }
    }
    return FALSE;
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    if (variable_get(media_dailymotion_variable_name('title_to_filename'))) {
      $values = explode('/', file_uri_target($uri));

      // Build api link for getting title and save it to file object.
      if (isset($file->filename) && isset($values[1]) && $file->filename == $values[1]) {
        $api_url = 'https://api.dailymotion.com/video/' . $file->filename . '?fields=title';
        $response = drupal_http_request($api_url);
        if (!isset($response->error)) {
          $data = drupal_json_decode($response->data);
          $file->filename = $data['title'];
        }
      }
    }

    return $file;
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }
}
