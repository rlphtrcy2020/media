
-------------------------------------------------------------------------------
                               Vimeo Video Uploader
-------------------------------------------------------------------------------

The Vimeo Video Uploader module allows users to Uploads videos to vimeo on
creation of content from your drupal site.

1.x version of the module requires older version of Vimeo API
2.x version of the module incorporates changes made in new and updated Vimeo API

Project homepage: http://drupal.org/project/vimeo_video_uploader


Prerequisite
-------------

 * Create a App at vimeo.com, visit <https://developer.vimeo.com/apps>
   Get the Created App Authenticated.
   (It takes few days to get authenticated by vimeo.com)
   Copy/Save the details.
    - Vimeo User Id.
    - Client ID.
    - Client Secret.
    - Access token.


Installation
-------------

 * Copy the whole vimeo video uploader directory to your modules directory
   (e.g. DRUPAL_ROOT/sites/all/modules) and activate the Vimeo Video Uploader
   module
 * Download the Library API of Vimeo from "https://github.com/vimeo/vimeo.php".
 * Go to the path "sites/all/libraries"
   Create a folder "vimeo-lib-api" and place the downloaded all file.
   to vimeo-lib-api folder (DRUPAL_ROOT/sites/all/libraries/vimeo-lib-api)
 * You should see
   (DRUPAL_ROOT/sites/all/libraries/vimeo-lib-api/vimeo.php/autoload.php).


Configuration
--------------

 * Configuration Page.
   http://yoursite.com/admin/config/media/vimeo_video_uploader
 * On module configuration page enter the copied/saved details of App from
   https://www.vimeo.com and select the content type from which you have to
   upload the video to Vimeo.
