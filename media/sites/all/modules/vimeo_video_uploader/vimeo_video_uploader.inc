<?php

/**
 * @file
 * Include file for the Vimeo Video Uploader.
 */

/**
 * Function provide form for Vimeo Video Uploader Configuration page.
 */
function vimeo_video_uploader_admin_form($form, &$form_state) {
  $form['vimeo_auth'] = array(
    '#type' => 'fieldset',
    '#title' => t('Vimeo Video Upload Configuration'),
    '#description' => t("Before Configuring, Read <a target='_blank' href='https://www.drupal.org/node/2726121'>Documentation page</a>."),
  );
  $form['vimeo_auth']['client_id'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('vimeo_video_uploader_client_id'),
    '#title' => t('Enter (Vimeo Client Identifier)'),
    '#required' => TRUE,
  );
  $form['vimeo_auth']['client_secret'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('vimeo_video_uploader_client_secret'),
    '#title' => t('Enter (Vimeo Client Secrets)'),
    '#required' => TRUE,
  );
  $form['vimeo_auth']['access_token'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('vimeo_video_uploader_access_token'),
    '#title' => t('Enter generated (Your new Access token)'),
    '#required' => TRUE,
  );
  $content_types = node_type_get_types();
  $content_types_val = array('' => "-Select-");
  foreach ($content_types as $type) {
    $content_types_val[$type->type] = $type->name;
  }
  $form['vimeo_auth']['content_type_select'] = array(
    '#type' => 'select',
    '#title' => t('Select the Content Types from which you have to upload video to Vimeo'),
    '#options' => $content_types_val,
    '#default_value' => variable_get('vimeo_video_uploader_content_type_select'),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}


/**
 * Function triggered on submit of Vimeo Video Uploader Configuration page.
 */
function vimeo_video_uploader_admin_form_submit($form, $form_state) {
  variable_set('vimeo_video_uploader_client_id', $form_state['values']['client_id']);
  variable_set('vimeo_video_uploader_client_secret', $form_state['values']['client_secret']);
  variable_set('vimeo_video_uploader_access_token', $form_state['values']['access_token']);
  $message = "Saved the Vimeo configuration.";
  $sel_con_type = $form_state['values']['content_type_select'];
  $exist_set_con_type = variable_get("vimeo_video_uploader_content_type_select");
  if ($sel_con_type != $exist_set_con_type) {
    field_delete_field("field_vimeo_file_browse");
    field_delete_field("field_embeddedvideo");
  }
  variable_set('vimeo_video_uploader_content_type_select', $sel_con_type);
  $instances = field_info_instances('node', $sel_con_type);
  $fields = array();
  foreach (array_keys($instances) as $field_instance) {
    $fields[$field_instance] = $field_instance;
  }
  if (!in_array("field_vimeo_file_browse", $fields)) {
    $field_name = 'field_vimeo_file_browse';
    $field = array(
      'field_name' => $field_name,
      'type' => 'file',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    );
    field_create_field($field);
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => 'node',
      'bundle' => $sel_con_type,
      'settings' => array('file_extensions' => 'mp4'),
      'display' => array(
        'default' => array('label' => 'hidden', 'type' => 'hidden'),
      ),
      'label' => 'Browse video for uploading to Vimeo',
    );
    field_create_instance($instance);
    $message .= "Created 'Browse video for uploading to Vimeo' field in '" . strtoupper($sel_con_type) . "' Content type.";
  }
  if (!in_array("field_embeddedvideo", $fields)) {
    $field_name = 'field_embeddedvideo';
    $field = array(
      'field_name' => $field_name,
      'type' => 'video_embed_field',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
    );
    field_create_field($field);
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => 'node',
      'bundle' => $sel_con_type,
    );
    field_create_instance($instance);
  }
  drupal_set_message(check_plain($message), 'status');
}
