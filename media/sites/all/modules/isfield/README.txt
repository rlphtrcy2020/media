Homepage http://drupal.org/project/isfield

This is a light weight module that provides an Internet Resource field (i.e.
emfield in its 6.x branch).

You need to download the MediaWrapper library and place it under /sites/all/libraries
(create this directory if it does not exist yet). Or you can place it anywhere
if the MediaWrapper class can be autoloaded.

